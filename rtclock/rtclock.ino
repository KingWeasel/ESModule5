/*
   rtclock.ino

   Jacob Kunnappally
   September 2017

   What this program does:

   Setup
   -define time structure
   -define constants
   -declare variables
   -start serial comms
   -configure and start timer (with interrupt)
   -configure output pins

   Runtime
   -Listen for user serial input
    -process received strings
   -react to timer interrupt with time struct increment
    and LED blink
   -provide clock out on external signal
*/

struct datetime
{
  int secs;
  short mins;
  short hours;
  short days;
} dtime;

#define BOARD_LED_PIN 13
#define PERIOD 1000000    // in microseconds, 1Hz toggles

#define MAX_SECS 60
#define MAX_MINS 60
#define MAX_HOURS 24
#define MAX_DAYS 365

#define DRDY_PIN 0
#define CLOCK_DATA_PIN 7
#define FRAME_BIT_WIDTH 1024
#define CLOCK_DATA_RATE 1888

#define STRING_MAX 200

// Example uses timer 2, so I will 2
HardwareTimer timer(3);

volatile bool led_state = true;

String inputString = "";         // a String to hold incoming data
boolean stringComplete = false;  // whether the string is complete

void setup()
{
  pinMode(BOARD_LED_PIN, OUTPUT);

  pinMode(DRDY_PIN, INPUT);
  attachInterrupt(DRDY_PIN, shift_in_data, RISING);
  pinMode(CLOCK_DATA_PIN, OUTPUT);

  // Pause the timer while we're configuring it
  timer.pause();

  // Set up period
  timer.setPeriod(PERIOD); // in microseconds

  // Set up an interrupt on channel 1
  //also straight from example
  timer.setChannel1Mode(TIMER_OUTPUT_COMPARE);
  timer.setCompare(TIMER_CH1, 1);  // Interrupt 1 count after each update
  timer.attachCompare1Interrupt(sec_handler);

  // Refresh the timer's count, prescale, and overflow
  timer.refresh();

  // Start the timer counting
  timer.resume();

  // initialize serial
  Serial.begin(115200);
  while (!Serial);
  print_help();
  Serial.flush();

  // reserve STRING_MAX bytes for the inputString
  inputString.reserve(STRING_MAX);
}

void loop()
{
  while (Serial.available() > 0)
  {
    inputString.concat((char)Serial.read());
  }

  if (inputString.length() > 0)
  {
    process_string(inputString);
    inputString.remove(0);
  }
}

void sec_handler(void)
{
  toggle_led();
  inc_time();
}

void toggle_led()
{
  led_state = !led_state;
  digitalWrite(BOARD_LED_PIN, led_state);
}

void inc_time()
{
  //I don't know if these inlines and prefixes make for more
  //efficient compilation, but I'm sure hoping they do!
  if (++dtime.secs >= MAX_SECS)
  {
    dtime.secs = 0;
    if (++dtime.mins >= MAX_MINS)
    {
      dtime.mins = 0;
      if (++dtime.hours >= MAX_HOURS)
      {
        dtime.hours = 0;
        if (++dtime.days >= MAX_DAYS)
        {
          dtime.days = 0;
        }
      }
    }
  }
}

void shift_in_data ()
{
  bool shift_clk = false;
  int i = 0;
  float period = (1.0 / CLOCK_DATA_RATE) * 1000000.0; //make us

  for (; i < FRAME_BIT_WIDTH; i++)
  {
    digitalWrite(CLOCK_DATA_PIN, shift_clk);
    shift_clk = !shift_clk;
    delayMicroseconds(period);
  }
  //  Serial.println(i);
  //  Serial.println(period);

  digitalWrite(CLOCK_DATA_PIN, false); //clear
}

void process_string(String inputString)
{
  String int_holder = "";
  int_holder.reserve(STRING_MAX);

  inputString.trim();

  if (inputString.equalsIgnoreCase("help"))
  {
    print_help();
  }
  else if (inputString.equalsIgnoreCase("time"))
  {
    print_time();
  }
  else if (inputString.indexOf(":") == 1)
  {
    int_holder = inputString.substring(2);

    if (int_holder.equals("0") || int_holder.toInt() != 0)
      set_date_param(inputString.charAt(0), int_holder.toInt());
    else
    {
      Serial.println("Unrecognized date time value. Printing help instead...");
      print_help();
    }
  }
  else
  {
    Serial.println("\nUnrecognized input. Printing help instead...");
    print_help();
  }
}

void print_help()
{
  Serial.println("\n|Help Menu|\n");
  Serial.println("Note, when entering commands, do not include the < or > brackets");
  Serial.println("These denote variable parameters\n");
  Serial.println("Print help - help");
  Serial.println("Print current time in DDD HH:MM:SS - time");
  Serial.println("Set days -  d:<1-365>");
  Serial.println("Set hours - h:<0-23>");
  Serial.println("Set minutes - m:<0-59>");
  Serial.println("Set seconds - s:<0-59>\n");
}

void print_time()
{
  Serial.print("Date and Time: ");
  if (dtime.days < 100)
  {
    zpad();
    if (dtime.days < 10)
      zpad();
  }
  Serial.print(dtime.days);
  Serial.print(" ");
  if (dtime.hours < 10)
    zpad();
  Serial.print(dtime.hours);
  Serial.print(":");
  if (dtime.mins < 10)
    zpad();
  Serial.print(dtime.mins);
  Serial.print(":");
  if (dtime.secs < 10)
    zpad();
  Serial.println(dtime.secs);
}

void set_date_param(char param, int val)
{
  bool invalid = false;

  noInterrupts();
  switch (param)
  {
    case 'd':
      if (val < 1 || val > 365)
        invalid = true;
      else
        dtime.days = val;

      break;

    case 'h':
      if (val < 0 || val > 23)
        invalid = true;
      else
        dtime.hours = val;

      break;

    case 'm':
      if (val < 0 || val > 59)
        invalid = true;
      else
        dtime.mins = val;

      break;

    case 's':
      if (val < 0 || val > 59)
        invalid = true;
      else
        dtime.secs = val;

      break;

    default:
      invalid = true;
      break;
  }

  interrupts();

  if (invalid)
    Serial.println("Value out of bounds or unrecognized. No changes made.");

  print_time();
}

void zpad()
{
  Serial.print("0");
}





